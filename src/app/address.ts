export class address{
    street: number
    suite: string
    city: string
    zipcode: string
    geo: string
    lat: string
    lng: string
}