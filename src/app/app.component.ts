import { Component, OnInit } from '@angular/core';
import { GetUserService } from './get-user.service';
import { UserFormComponent } from './user-form/user-form.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'angularDay2';
  userModel
  selectedID
  selectedUser

  myForm: UserFormComponent 

  constructor(private myGetUserService: GetUserService){
  }

  ngOnInit(){
    this.myGetUserService.getSymbol().subscribe( (results) => {
      this.userModel = results
      this.selectedUser = results[0]
    })
  }

   onConfirm(event: any){
     this.myGetUserService.getSelectedUser(this.selectedID).subscribe( (result) => {
       this.selectedUser = result
     })
   }
  
}
