import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { user } from './user';

@Injectable({
  providedIn: 'root'
})
export class GetUserService {

  userUrl = ' https://jsonplaceholder.typicode.com/users'

  currentUser

  combined

  constructor(private http: HttpClient) { }

  getSymbol():Observable<user[]>{
    return this.http.get<user[]>(this.userUrl)
  }

  getSelectedUser(id): Observable<user>{
    this.currentUser = id
    this.combined =`${this.userUrl}/${this.currentUser}`
    console.log('full url'+this.combined)
    return this.http.get<user>(this.combined)

  }
}
